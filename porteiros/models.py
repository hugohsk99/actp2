from django.db import models

# Create your models here.
class Porteiro(models.Model):

    usuario = models.OneToOneField(
        "app.Usuario",
        verbose_name="Usuario",
        on_delete=models.PROTECT,
    )

    nome_completo = models.CharField(
        verbose_name="Nome completo",
        max_length=194,

    )

    cpf = models.CharField(
        verbose_name="CPF",
        max_length=11,
    )

    telefone = models.CharField(
        verbose_name="Telefone de Contato",
        max_length=11,
    )

    data_nascimento = models.DateField(
        verbose_name="Data de nascimento",
        auto_now_add=False,
        auto_now=False,
    )

    class Meta:
        verbose_name = "Porteiro"
        verbose_name_plural = "Porteiros"
        db_table = "porteiro"

    def _str_(self):
        return self.nome_completo